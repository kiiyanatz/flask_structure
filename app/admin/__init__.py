from flask import Blueprint

admin = Blueprint('admin', __name__,
                  static_folder='admin_static',
                  template_folder='admin_templates')


from . import views
