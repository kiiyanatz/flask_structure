from flask import Blueprint

core = Blueprint('root', __name__)

from . import views, errors